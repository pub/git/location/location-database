# IPFire Location Database

This is the official Git repository of the IPFire Location Database, a free
Geolocation database for the internet. For more information, please check out
our website at: https://www.ipfire.org/location

The data is licensed under Creative Commons BY-NC-SA 4.0.

## Notes On Usage

This Git repository exists for audit reasons only. It is not supposed to be parsed
by any application as the data is not stored in a machine friendly-format and hard
to parse correctly.

The published database is compiled into a highly optimised file format which is
cryptographically signed, extremely fast to search through and small. Packages to
validate and parse the data are available for a variety of distributions and anyone
using this database is **stronly** encouraged to only use those.

## Report A Problem

If you find a problem in this database, please report it back to us at:

  https://www.ipfire.org/location/report-a-problem
